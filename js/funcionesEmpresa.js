const llenarTabla = async () => {
    const htmlBody = document.getElementById("body-empresas");
    const listaEmpresas = await getEmpresas();
    for (let i = 0; i < listaEmpresas.length; i++) {
        const empresa = listaEmpresas[i];
        
        let tr = document.createElement("tr");
        let th = document.createElement("th");
        th.scope = "row";
        th.innerHTML = empresa.id;

        let tdNombre = document.createElement("td");
        tdNombre.innerHTML = empresa.nombre;

        let tdCuit = document.createElement("td");
        tdCuit.innerHTML = empresa.cuit;
        
        let imgEliminar = document.createElement("img");
        imgEliminar.src = "../assets/icons/trash.svg";
        imgEliminar.className = "icons";
        
        let imgVer = document.createElement("img");
        imgVer.src = "../assets/icons/eye.svg";
        imgVer.className = "icons";

        let aEliminar = document.createElement("a");
        aEliminar.href = "#";
        aEliminar.setAttribute("onclick","eliminarEmpresa(this)");
        aEliminar.appendChild(imgEliminar);

        let aVer = document.createElement("a");
        aVer.href = "../pages/verEmpresa.html?id=" + empresa.id;
        aVer.appendChild(imgVer);

        let tdOpciones = document.createElement("td");
        tdOpciones.appendChild(aVer);
        tdOpciones.appendChild(aEliminar);

        tr.appendChild(th);
        tr.appendChild(tdNombre);
        tr.appendChild(tdCuit);
        tr.appendChild(tdOpciones);

        htmlBody.appendChild(tr);
    }
}

const crearEmpresa = async () => {

    const nombreHtml = document.getElementById("nombre");
    const cuitHtml = document.getElementById("cuit");
    
    await createEmpresa(nombreHtml.value, cuitHtml.value);
    setTimeout(() => {
        window.location.href = "./index.html";
    }, 3000);
}

const eliminarEmpresa = async (element) => {
    const th = element.parentNode.parentNode.getElementsByTagName("th")[0];
    const id = th.innerHTML;

    await deleteEmpresa(id);
    th.parentNode.remove();
}

const verEmpresa = async () => {
    const id = getUrlParameter("id");
    const empresa = await getEmpresaById(id);
    
    const nombreHtml = document.getElementById("nombre");
    const cuitHtml = document.getElementById("cuit");

    nombreHtml.value = empresa.nombre;
    cuitHtml.value = empresa.cuit;

    const btnAgregar = document.getElementById("btn-agregar");
    btnAgregar.setAttribute("onclick",`redirigirCrearSucural(${id})`);

    const htmlBody = document.getElementById("body-empresas");

    for (let i = 0; i < empresa.sucursales.length; i++) {
        const sucursal = empresa.sucursales[i];
        
        let tr = document.createElement("tr");
        let th = document.createElement("th");
        th.scope = "row";
        th.innerHTML = sucursal.id;

        let tdNombre = document.createElement("td");
        tdNombre.innerHTML = sucursal.nombre;

        let tdDireccion = document.createElement("td");
        tdDireccion.innerHTML = sucursal.direccion;

        let imgEliminar = document.createElement("img");
        imgEliminar.src = "../assets/icons/trash.svg";
        imgEliminar.className = "icons";
        
        let imgVer = document.createElement("img");
        imgVer.src = "../assets/icons/eye.svg";
        imgVer.className = "icons";

        let aEliminar = document.createElement("a");
        aEliminar.href = "#";
        aEliminar.setAttribute("onclick",`eliminarSucursal(${sucursal.id})`);
        aEliminar.appendChild(imgEliminar);

        let aVer = document.createElement("a");
        aVer.href = `../pages/verSucursal.html?id=${sucursal.id}&empresa=${empresa.id}`;
        aVer.appendChild(imgVer);

        let tdOpciones = document.createElement("td");
        tdOpciones.appendChild(aVer);
        tdOpciones.appendChild(aEliminar);

        tr.appendChild(th);
        tr.appendChild(tdNombre);
        tr.appendChild(tdDireccion);
        tr.appendChild(tdOpciones);

        tr.id = sucursal.id;

        htmlBody.appendChild(tr);
    }
}

const editarEmpresa = async () => {
    const nombreHtml = document.getElementById("nombre");
    const cuitHtml = document.getElementById("cuit");
    const id = getUrlParameter("id");

    await updateEmpresa(id, nombreHtml.value, cuitHtml.value);

    window.alert("Se actualizó con exito");

    nombreHtml.disabled = true;
    cuitHtml.disabled = true;

    const btnGuardar = document.getElementById("btn-guardar");
    btnGuardar.style.display = "none";

    const btnEditar = document.getElementById("btn-editar");
    btnEditar.disabled = false;
    btnEditar.style.backgroundColor = "white";
    btnEditar.style.color = "#0dcaf0";
}

const verSucursal = async () => {
    const id = getUrlParameter("id");
    const sucursal = await getSucursalById(id);
    
    const nombreHtml = document.getElementById("nombre");
    const direccionHtml = document.getElementById("direccion");
    const gerenteHtml = document.getElementById("gerente");
    const personalHtml = document.getElementById("personal");

    nombreHtml.value = sucursal.nombre;
    direccionHtml.value = sucursal.direccion;
    gerenteHtml.value = sucursal.gerente;
    personalHtml.value = sucursal.personal;

    const empresaId = getUrlParameter("empresa");
    const btnVolver = document.getElementById("volver");
    btnVolver.href = `./verEmpresa.html?id=${empresaId}`;
}

const editarSucursal = async () => {
    const nombreHtml = document.getElementById("nombre");
    const direccionHtml = document.getElementById("direccion");
    const gerenteHtml = document.getElementById("gerente");
    const personalHtml = document.getElementById("personal");
    const id = getUrlParameter("id");

    const body = {
        nombre: nombreHtml.value,
        direccion: direccionHtml.value,
        gerente: gerenteHtml.value,
        personal: personalHtml.value,
    };

    await updateSucursal(id, body);

    window.alert("Se actualizó con exito");

    nombreHtml.disabled = true;
    nombreHtml.disabled = true;
    direccionHtml.disabled = true;
    gerenteHtml.disabled = true;
    personalHtml.disabled = true;

    const btnGuardar = document.getElementById("btn-guardar");
    btnGuardar.style.display = "none";

    const btnEditar = document.getElementById("btn-editar");
    btnEditar.disabled = false;
    btnEditar.style.backgroundColor = "white";
    btnEditar.style.color = "#0dcaf0";
}

const eliminarSucursal = async (id) => {
    if (!id || isNaN(id)) {
        window.alert(`El id "${id} no es valido`);
        return;
    }

    const respuesta = await deleteSucursal(id);

    window.alert(respuesta.mensaje);

    const nodo = document.getElementById(id);
    nodo.remove();
}