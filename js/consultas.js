const urlBase = 'http://localhost:3000';

const getEmpresas = async () => {
    const parametros = {
        method: 'get',
        url: `${urlBase}/empresa`,
        responseType: 'json'
    };
    let empresas;
    await axios(parametros)
        .then( response => {
            empresas = response.data;
        })
        .catch( error => {
            console.log(error)
        });

    return empresas;
}

const createEmpresa = async (nombreEmpresa, cuitEmpresa) => {
    const body = {
        nombre: nombreEmpresa,
        cuit: cuitEmpresa
    };

    await axios.post(`${urlBase}/empresa/create`, body);
}

const getEmpresaById = async (empresaId) => {
    if (!empresaId || isNaN(empresaId)){
        window.alert("No es un id valido");
    } else {
        const parametros = {
            url: `${urlBase}/empresa/id?id=${empresaId}`,
            method: "get"
        };

        const result = await axios.request(parametros);
        return result.data;
    }
}

const deleteEmpresa = async (id) => {
    if (!id || isNaN(id)){
        window.alert("No es un id valido");
    } else {
        await axios.delete(`${urlBase}/empresa/delete/${id}`);
    }
}

const updateEmpresa = async (id, nombreInput, cuitInput) => {
    const body = {
        nombre: nombreInput,
        cuit: cuitInput
    };

    await axios.patch(`${urlBase}/empresa/update/${id}`, body);
}

const getSucursalById = async (sucursalId) => {
    if (!sucursalId || isNaN(sucursalId)){
        window.alert("No es un id valido");
    } else {
        const parametros = {
            url: `${urlBase}/sucursal/${sucursalId}`,
            method: "get"
        };

        const result = await axios.request(parametros);
        return result.data;
    }
}

const updateSucursal = async (id, body) => {
    console.log(id, body)
    await axios.patch(`${urlBase}/sucursal/${id}`, body);
}

const deleteSucursal = async (id) => {
    const result = await axios.delete(`${urlBase}/sucursal/${id}`);
    return result.data;
}
